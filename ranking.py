

def seperate_out(lst:list[list],number:int):
    for i in lst:
        for j in lst:
            if(all((i[a]) > (j[a])   for a in range(1,number+1)) or all(i[a]<j[a]   for a in range(0,number))):
                i.extend(j)
                lst.remove(j) 
    return lst
def assign_rank(lst:list[list],number):
    ans_list = []
    for i in lst:
        new_list = []
        for j in range(0,len(i),number+1):
            new_list.append([i[j]] + [sum(i[l] for l in range(j+1,j+number+1) )] )
        
        new_list.sort(key=lambda x: x[1])
        ans_list.append(new_list)
    return ans_list
def read_file(filenmae:str):
    lst=[]
    for line in open (filenmae):
        name,marks = line.split()[0],line.split()[1:]
        marks = [int(_) for _ in marks ]
        number = len(marks)
        lst.append([name] + marks )
    return lst,number

lst,number = read_file("data.txt")
seperate_out(lst,number)

ans_list=assign_rank(lst,number)
ans=""
for lists in ans_list:
    for students in lists:
        ans += str(students[0]) +"<"
    ans = ans[:-1]
    ans += "\n"
print(ans)